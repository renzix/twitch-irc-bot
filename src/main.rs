extern crate irc;
extern crate chrono;

use irc::client::prelude::*;
//Time
use chrono::Local;
use chrono::DateTime;
use std::time::SystemTime;
//Read from file for oauth string
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::ffi::OsStr;

fn main() {
    //Connect to twitch
    let config = Config {
        nickname: Some("therenzix".to_owned()),
        server: Some("irc.twitch.tv".to_owned()),
        port: Some(6667),
        password: get_oauth(Path::new("oauth.txt").as_os_str()),
        channels: Some(vec!["#therenzix".to_owned(), "#feviknight".to_owned()]),
        ..Config::default()
    };
    let mut reactor = IrcReactor::new().unwrap();
    let client = reactor.prepare_client_and_connect(&config).unwrap();
    client.identify().unwrap();

    reactor.register_client_with_handler(client, |client, message| {
        let user = message.source_nickname().unwrap_or("Server");
        let time = SystemTime::now();
        let time: DateTime<Local> = time.into();
        match message.command {
            Command::PRIVMSG(ref target,ref msg) => {
                println!("{}_{}\t{}:{}", target,time.format("%T"),user,msg);
                if user=="therenzix" {
                    let msg: Vec<String> = msg.split(' ').map(|s| s.to_string()).collect();
                    if msg[0]=="!reninfo"{
                        client.send_privmsg(target, "I made my acc a bot with IRC and Rust cuz i want charms for feviknight!!!")?;
                    } else if msg[0]=="!rentime"{
                        client.send_privmsg(target, time.format("%T EST").to_string().as_str())?;
                    } else if msg[0]=="!renjoin"{
                        let mut opt = msg[1].chars();
                        let mut buildable = String::new();
                        let first = opt.next().unwrap();
                        if first != '#'{
                            buildable.push('#');
                        }
                        buildable.push(first.to_ascii_lowercase());
                        for c in opt{
                            buildable.push(c.to_ascii_lowercase());
                        }
                        let opt = buildable.to_string();
                        match client.send_join(&opt){
                            Ok(_) => (),
                            Err(_) => client.send_privmsg(target, format!("Couldnt find {}",msg[1]).as_str())?,
                        }
                    } else if msg[0]=="!renpart"{
                        if msg[1] != "all"{
                            let mut opt = msg[1].chars();
                            let mut buildable = String::new();
                            let first = opt.next().unwrap();
                            if first != '#'{
                                buildable.push('#');
                            }
                            buildable.push(first.to_ascii_lowercase());
                            for c in opt{
                                buildable.push(c.to_ascii_lowercase());
                            }
                            let opt = buildable.to_string();
                            match client.send_part(opt.as_str()){
                                Ok(_) => (),
                                Err(_) => client.send_privmsg(target, format!("Error parting with {}", msg[1]).as_str())?,
                            }
                        }else{
                            //TODO Part with everything
                        }
                    } else if msg[0]=="!renexit"{
                        exit(client, target);
                    }
                }
            }
            _ => (),
        }
        Ok(())
    });

    reactor.run().unwrap();

    //profit
}

// Cuz i cant give away my oauth code u have to use ur own
// go here https://twitchapps.com/tmi and put it into the same
// dir as your Cargo.toml (or a custom one by changing the path above)
fn get_oauth(os_str: &OsStr) -> Option<String>{
    let mut file = File::open(os_str).ok().unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).ok();
    Some(contents.trim().to_string())
}


fn exit(client: &IrcClient , target: &str){
    client.send_privmsg(target, "Bot shuting down now").ok();
    std::process::exit(0);
}
